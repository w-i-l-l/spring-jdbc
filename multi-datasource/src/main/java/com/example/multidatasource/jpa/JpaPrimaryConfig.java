package com.example.multidatasource.jpa;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJdbcRepositories(
        basePackages = {"com.example.multidatasource.jpa.primaryRepository"}
)
// 需要配置EntityManger 、PlatformTransactionManager
public class JpaPrimaryConfig {

    @Autowired
    private DataSource dataSource;


    @Bean
    @Primary
    public EntityManager primaryEntityManager(){

        //todo j

        return null;



    }
}