package com.example.multidatasource.jdbc;


import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@Slf4j
public class JdbcTemplateTest {


    @Autowired
    private JdbcTemplate primaryJdbcTemple;


    @Autowired
    @Qualifier("secondaryJdbcTemplate")
    private JdbcTemplate secondaryJdbcTemplate;


    public void QueryTest(){
        log.info("primaryJdbcTemple QueryTest 使用");
        primaryJdbcTemple.queryForList("select * from user").forEach(row->{log.info(row.toString());});
        log.info("secondaryJdbcTemplate QueryTest 使用");
        primaryJdbcTemple.queryForList("select * from user").forEach(row->{log.info(row.toString());});
    }

    public void updateTest(){
        log.info("primaryJdbcTemple  updateTest使用");
        secondaryJdbcTemplate.update("insert into user (id,name) values (?,?)",Math.abs(Math.random()*10000), UUID.randomUUID().toString().substring(0,10));
        log.info("secondaryJdbcTemplate updateTest 使用");
        secondaryJdbcTemplate.update("insert into user (id,name) values (?,?)",Math.abs(Math.random()*10000), UUID.randomUUID().toString().substring(0,10));
        QueryTest();
    }


}
