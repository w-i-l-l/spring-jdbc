package com.example.multidatasource.jdbc;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

@Configuration
@Slf4j
public class JdbcTemplateConfig {





    @Bean
    @Primary
    public JdbcTemplate primaryJdbcTempale(@Autowired DataSource dataSource){
        return new JdbcTemplate(dataSource);
    }


    @Bean(name = "secondaryJdbcTemplate")
    public  JdbcTemplate secondaryJdbcTemplate(@Qualifier(value = "secondaryDataSource") DataSource dataSource){
        return  new JdbcTemplate(dataSource);
    }
}
