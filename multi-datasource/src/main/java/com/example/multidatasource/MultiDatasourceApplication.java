package com.example.multidatasource;

import com.example.multidatasource.jdbc.JdbcTemplateTest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.sql.DataSource;

@SpringBootApplication
@Slf4j
public class MultiDatasourceApplication implements ApplicationRunner {


	@Autowired
	private DataSource primaryDatasource;

	@Autowired
	@Qualifier(value = "secondaryDataSource")
	private DataSource secondaryDataSource;

	@Autowired
	private JdbcTemplateTest jdbcTemplateTest;

	public static void main(String[] args) {
		SpringApplication.run(MultiDatasourceApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		log.info(primaryDatasource.toString());
		log.info(secondaryDataSource.toString());

		jdbcTemplateTest.QueryTest();
		jdbcTemplateTest.updateTest();

	}
}
