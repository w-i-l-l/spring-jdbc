package com.example.datasource;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;
import java.util.Map;

@Slf4j
@SpringBootApplication
public class DatasourceApplication implements ApplicationRunner {

	@Autowired
	private DataSource dataSource;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public static void main(String[] args) {
		SpringApplication.run(DatasourceApplication.class, args);
	}


	@Override
	public void run(ApplicationArguments args) throws Exception {
		log.info(dataSource.toString());
		log.info(jdbcTemplate.toString());

		List<Map<String, Object>> mapList = jdbcTemplate.queryForList("select * from user");
		mapList.stream().forEach(row->{
			log.info(row.toString());
		});



	}
}
