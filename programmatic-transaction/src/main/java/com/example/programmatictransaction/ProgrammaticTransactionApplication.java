package com.example.programmatictransaction;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.DefaultTransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.sql.DataSource;

@SpringBootApplication
@Slf4j
public class ProgrammaticTransactionApplication implements ApplicationRunner {

	@Autowired
	private DataSource dataSource;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public TransactionTemplate txTemplate;


	@Autowired
	private PlatformTransactionManager txManger;

	public static void main(String[] args) {
		SpringApplication.run(ProgrammaticTransactionApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {

		txDemo2();
	}

	private void txDemo1() {
		try{
			txTemplate.setIsolationLevel(TransactionTemplate.ISOLATION_REPEATABLE_READ);
			txTemplate.setPropagationBehavior(TransactionTemplate.PROPAGATION_REQUIRED);
			txTemplate.execute(new TransactionCallbackWithoutResult(){

				@Override
				protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
					jdbcTemplate.update("insert into user(id,name) values (3,'张三');");
					log.info("count"+getCount());
					int a=1/0;
				}
			});
		}catch (RuntimeException e){
			log.error("啊多发点",e);
		}

		log.info("+++++++++++++++++++++++++++++++++++++++++");
		log.info("count"+getCount());
	}


	private void txDemo2(){
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(DefaultTransactionDefinition.PROPAGATION_REQUIRED);
		def.setIsolationLevel(DefaultTransactionDefinition.ISOLATION_REPEATABLE_READ);
		TransactionStatus status = txManger.getTransaction(def);
		try{
			jdbcTemplate.update("insert into user(id,name) values (3,'张三');");
			log.info("count"+getCount());
			int a=1/0;
			txManger.commit(status);
		}catch (RuntimeException e){
			log.error("sdf",e);
			txManger.rollback(status);
		}
		log.info("=======================================");
		log.info("count"+getCount());

	}


	private int getCount(){
		return Integer.parseInt(jdbcTemplate.queryForList("select count(1) as count from user").get(0).get("count").toString());
	}

}
