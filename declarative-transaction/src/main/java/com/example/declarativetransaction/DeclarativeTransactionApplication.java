package com.example.declarativetransaction;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class DeclarativeTransactionApplication implements ApplicationRunner {

	@Autowired
	private  UserSerivce userSerivce;

	public static void main(String[] args) {
		SpringApplication.run(DeclarativeTransactionApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {

		try {
			userSerivce.insert();
		} catch (Exception e) {
			e.printStackTrace();
		}
		int count = userSerivce.count();

		log.info("--------------------------------------------");
		log.info("count"+count);
	}
}
