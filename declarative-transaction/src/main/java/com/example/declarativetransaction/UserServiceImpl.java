package com.example.declarativetransaction;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class UserServiceImpl implements UserSerivce {

    /**
     * PROPAGATION_REQUIRED 	如果当前没有事务，就新建一个事务，如果已经存在一个事务中，加入到这个事务中。这是最常见的选择。
     * PROPAGATION_SUPPORTS 	支持当前事务，如果当前没有事务，就以非事务方式执行。
     * PROPAGATION_MANDATORY 	使用当前的事务，如果当前没有事务，就抛出异常。
     * PROPAGATION_REQUIRES_NEW 	新建事务，如果当前存在事务，把当前事务挂起。
     * PROPAGATION_NOT_SUPPORTED 	以非事务方式执行操作，如果当前存在事务，就把当前事务挂起。
     * PROPAGATION_NEVER 	以非事务方式执行，如果当前存在事务，则抛出异常。
     * PROPAGATION_NESTED 	如果当前存在事务，则在嵌套事务内执行。如果当前没有事务，则执行与PROPAGATION_REQUIRED类似的操作。
     *
     *
     *
     * DEFAULT 	使用数据库本身使用的隔离级别
     * ORACLE（读已提交） MySQL（可重复读）
     * READ_UNCOMITTED 	读未提交（脏读）最低的隔离级别，一切皆有可能。
     * READ_COMMITED 	读已提交，ORACLE默认隔离级别，有幻读以及不可重复读风险。
     * REPEATABLE_READ 	可重复读，解决不可重复读的隔离级别，但还是有幻读风险。
     * SERLALIZABLE 	串行化，最高的事务隔离级别，不管多少事务，挨个运行完一个事务的所有子事务之后才可以执行另外一个事务里面的所有子事务，这样就解决了脏读、不可重复读和幻读的问题了
     */


    @Autowired
    private  JdbcTemplate jdbcTemplate;


    @Autowired
    private ApplicationContext ctx;




    @Transactional(propagation= Propagation.REQUIRED,isolation= Isolation.READ_COMMITTED,rollbackFor = RuntimeException.class)
    @Override
    public void insert() {
        jdbcTemplate.update("insert into user(id,name) values (3,'张三');");
        int count = ctx.getBean(UserSerivce.class).count();
        log.info("count"+count);
        int a=1/0;

    }

    @Transactional(propagation= Propagation.REQUIRED,isolation= Isolation.READ_COMMITTED,rollbackFor = RuntimeException.class) //改变propagation看效果
    @Override
    public int count() {
        return Integer.parseInt((String.valueOf(jdbcTemplate.queryForList("select count(1) as count from user").get(0).get("count"))));
    }
}
